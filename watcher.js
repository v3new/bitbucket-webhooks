var url = require('url');
var http = require('http');
var exec = require('child_process').exec;
var repos = require('./repos.json')
var spawn = require('child_process').spawn;


function puts(error, stdout, stderr) { 
    console.log('ERROR: ' + error);
    console.log('STDOUT: ' + stdout);
    console.log('STDERR: ' + stderr);
}

function getDateTime() {
    var date = new Date();
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var day  = date.getDate();
    var hour = date.getHours();
    var min  = date.getMinutes();
    var sec  = date.getSeconds();
    month = (month < 10 ? "0" : "") + month;
    day = (day < 10 ? "0" : "") + day;
    hour = (hour < 10 ? "0" : "") + hour;
    min = (min < 10 ? "0" : "") + min;
    sec = (sec < 10 ? "0" : "") + sec;
    return year + ":" + month + ":" + day + " " + hour + ":" + min + ":" + sec;
}

function checkIPinWhiteList(ip) {
    var array_ip = ip.match(/\d+/g);

    switch (array_ip[0]) {
        case "131":
            if (array_ip[1] == "103" && array_ip[2] == "20" && array_ip[3] >= "161" && array_ip[3] <= "190") return true;
            break;
        case "165":
            if (array_ip[1] == "254" && array_ip[2] == "145" && array_ip[3] >= "1" && array_ip[3] <= "62") return true;
            break;
        case "104":
            if (array_ip[1] == "192" && array_ip[2] == "143") return true;
            break;
        default:
            return false;
    }
}

http.createServer(function (req, res) {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress ||
        req.socket.remoteAddress || req.connection.socket.remoteAddress;

    console.log("[" + getDateTime() + "] " + ip + " " + url.parse(req.url).pathname);


    req.on('data', function(chunk) {
        if (req.method == "POST" && url.parse(req.url).pathname == "/pull" && checkIPinWhiteList(ip))
        {
            var repo_name = JSON.parse(chunk.toString()).repository.name;
            var deploySh = spawn('bash', [ repos[repo_name] ], {
                cwd: process.env.HOME + '/modelme/account-mm-client',
            });
            deploySh.stdout.on('data', function (data) {    // register one or more handlers
                console.log(data.toString());
            });

            deploySh.stderr.on('data', function (data) {
                console.log(data.toString());
            });

            deploySh.on('exit', function (code) {
                console.log('deploy finished with code ' + code);
            });
            console.log("[" + getDateTime() + "] git pull to: " + repo_name);
        }
    });

    res.writeHead(200);
    res.end('');

}).listen(8000);
