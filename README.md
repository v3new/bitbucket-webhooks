# Запуск скрипта
```sh
nohup node watcher.js > output.log &
```

# Настройка Watcher'a (файл repos.json)
Каждый ключ является названием репозитория, значение - bash командой или путем к bash скрипту.

# Настройка Bitbucket'a 
[https://bitbucket.org/**username**/**reponame**/admin/addon/admin/bitbucket-webhooks/bb-webhooks-repo-admin](https://bitbucket.org/username/reponame/admin/addon/admin/bitbucket-webhooks/bb-webhooks-repo-admin)
```sh
URL: %url_to_server%:8000/pull
```